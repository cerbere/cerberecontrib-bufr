# -*- coding: utf-8 -*-
"""
Dataset class for CWDP CFOSAT L2B scatterometer BUFR format.
"""
import logging
import os
import re

import numpy as np
import xarray as xr

from cerberecontrib_bufr.dataset.bufrdataset import BUFRDataset


class CFOSCATL2BBUFRDataset(BUFRDataset):
    """Dataset class to read CWDP CFOSAT L2B  scatterometer BUFR files"""
    def _open_dataset(self, *args, **kwargs):
        if 'BUFR_TABLES' not in os.environ:
            raise ValueError(
                'BUFR_TABLES must be set and link to CFOSAT BUFR tables.'
            )
        logging.info('BUFR tables: {}'.format(os.environ['BUFR_TABLES']))

        dataset = super(CFOSCATL2BBUFRDataset, self)._open_dataset(
            *args, **kwargs
        )

        # drop useless fields
        dataset = dataset.drop([
            'attenuation_correction_of_sigma_0__from_tb',
            'antenna_polarisation_dum0',
            'antenna_polarisation_dum1',
            'brightness_temperature_0',
            'brightness_temperature_1',
            'standard_deviation_brightness_temperature_0',
            'standard_deviation_brightness_temperature_1',
            'second_0',
            'total_number__with_respect_to_accumulation_or_average_0',
            'total_number__with_respect_to_accumulation_or_average_1',
        ])

        # rename seawinds fields
        seawinds = {_: _.replace('seawinds', 'cfosat')
                    for _ in dataset.data_vars if 'seawinds' in _}
        dataset = dataset.rename(seawinds)

        # combine some arrays
        fields = list(dataset.data_vars.keys())
        combined = {}
        for f in fields:
            m = re.match(r'' + "(\\w{1,})_(\\d+)$", f)
            if m is not None:
                fcomp = (f, int(m.groups()[1]))
                if m.groups()[0] in combined:
                    combined[m.groups()[0]].append(fcomp)
                else:
                    combined[m.groups()[0]] = [fcomp]
        for f in combined:
            if len(combined[f]) == 4:
                dim = 'ambiguity'
            elif len(combined[f]) == 18:
                dim = 'view'
            else:
                raise ValueError(
                    "Unexpect number of subarrays for {}: {}".format(
                        f, len(combined[f]))
                )
            arr = xr.concat(
                [dataset[_[0]]
                 for _ in sorted(combined[f], key=lambda x: x[1])],
                dim=dim
            )
            if f in ['lat', 'lon']:
                dataset = dataset.assign({'view_' + f: arr})
            else:
                dataset = dataset.assign({f: arr})
            dataset = dataset.drop([_[0] for _ in combined[f]])

        # fix types
        for f in [
            'number_of_vector_ambiguities',
            'index_of_selected_wind_vector',
            'along_track_row_number',
            'satellite_sensor_indicator',
            'time_difference_qualifier',
            'total_number_of_sigma_0_measurements',
            'number_of_inner_beam_sigma_0__forward_of_satellite',
            'antenna_polarisation',
            'cfosat_sigma_0_quality',
            'cfosat_sigma_0_mode',
            'cfosat_land_ice_surface_type',
            'probability_of_rain',
            'cfosat_nof_rain_index',
        ]:
            #print(f, dataset[f].data.max())
            dataset[f] = dataset[f].astype('int')
            dataset[f].encoding['_FillValue'] = -9223372036854775808

        # fix attributes
        for att in self._as_attributes():
            dataset.attrs[att] = int(dataset.attrs[att])

        return dataset

    def get_values(self, fieldname, **kwargs):
        values = super(CFOSCATL2BBUFRDataset, self).get_values(fieldname, **kwargs)
        if fieldname != 'time' and '_FillValue' in self._std_dataset[fieldname].encoding:
            values = np.ma.masked_equal(
                values,
                self._std_dataset[fieldname].encoding['_FillValue'],
                copy=False
            )
        return values

    def _as_attributes(self):
        """
        Return the list of BUFR fields that should be considered as global
        attributes rather than data
        """
        return [
            'satellite_identifier',
            'cross_track_resolution',
            'along_track_resolution',
            'orbit_number',
            'software_identification__see_note_2',
            'wind_scatterometer_geophysical_model_function',
        ]

    def _add_count_suffix(self, newname, counter):
        if newname in ['lat', 'lon', 'second']:
            if counter == 0:
                return newname
            newname += '_{}'.format(str(counter - 1))
            return newname
        elif newname in ['antenna_polarisation']:
            if counter == 0:
                return 'antenna_polarisation_dum0'
            elif counter == 1:
                return 'antenna_polarisation_dum1'
            else:
                newname += '_{}'.format(str(counter - 2))
            return newname
        else:
            return super(CFOSCATL2BBUFRDataset, self)._add_count_suffix(
                newname, counter
            )
