# -*- coding: utf-8 -*-
"""
Dataset classs for KNMI L2 scatterometer BUFR format.

Requires the following packages:
  * https://github.com/pytroll/python-bufr

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""

from cerberecontrib_bufr.dataset.bufrdataset import BUFRDataset


class KNMIScatBUFRDataset(BUFRDataset):
    """Dataset class to read KNMI L2 scatterometer BUFR files"""

    def _as_attributes(self):
        """
        Return the list of BUFR fields that should be considered as gobal
        attributes rather than data
        """
        return [
            'satellite_identifier',
            'cross_track_resolution',
            'along_track_resolution',
            'orbit_number'
            ]
