# -*- coding: utf-8 -*-
"""
Dataset classs for BUFR format
"""
from collections import OrderedDict, Counter
import datetime

import numpy
import xarray as xr

from pybufr_ecmwf.bufr import BUFRReader

from cerbere.dataset.dataset import Dataset


UNITS = {'DEGREE TRUE': 'degree_true',
         'SECOND': 's',
         'M/S': 'm s-1',
         'M': 'm',
         'KG/(M**2)S': 'kg m-2 s',
         'K': 'K',
         'dB': 'dB',
         'NUMERIC': None,
         'DEGREE': 'degree'}

REFERENCE_TIME = datetime.datetime(1991, 1, 1, 0, 0, 0, 0)

FILLVALUE = 1.7e38

BUFR_FIELD_MATCHING = {
    'LATITUDE (HIGH ACCURACY)': 'lat',
    'LONGITUDE (HIGH ACCURACY)': 'lon',
    'LATITUDE (COARSE ACCURACY)': 'lat',
    'LONGITUDE (COARSE ACCURACY)': 'lon',
}
BUFR_DIM_MATCHING = {
    'dim_rec': 'row',
    'dim_entry': 'cell',
}

class BUFRDataset(Dataset):
    """Dataset class to read BUFR files.

    It requires the pybufr-ecmwf package.

    Requires the BUFR tables from emos package in the `BUFR_TABLES` environment
    variable, e.g.

    .. code block: bash

        export BUFR_TABLES=/opt/lib/emos/bufrtables/
    """
    def __init__(self, *args,
                 field_matching=BUFR_FIELD_MATCHING,
                 dim_matching=BUFR_DIM_MATCHING,
                 **kwargs):
        super().__init__(
            *args,
            field_matching=field_matching,
            dim_matching=dim_matching,
            **kwargs
        )

    def _as_attributes(self):
        """
        Return the list of BUFR fields that should be considered as global
        attributes rather than data
        """
        return []

    def _add_count_suffix(self, newname, counter):
            newname += '_{}'.format(str(counter))
            return newname

    def _rename(self, name, counter, total_count):
        if name in self._native2std_field:
            newname = self._native2std_field[name]
        else:
            newname = (
                name.lower()
                .replace(' ', '_')
                .replace('/', '_')
                .replace('(', '_')
                .replace(')', '_')
                .replace('-', '_')
                .strip('_')
                .replace('*', '')
            )

        # add suffix to duplicate names
        if total_count > 1:
            newname = self._add_count_suffix(newname, counter[name])
            counter[name] += 1

        return newname

    def _open_dataset(self, **kwargs) -> 'xr.Dataset':
        # open the BUFR content reader
        _handler = BUFRReader(self._url)

        # read file content
        fieldnames = []
        fieldindices = []
        units = []
        data = OrderedDict([])
        attrs = {}

        recdata = []
        irec = 0

        for record in _handler:
            irec += 1
            ientry = 0
            for entry in record:
                ientry += 1
                if len(fieldnames) == 0:

                    names = entry.names
                    duplicates = Counter(names)
                    counter = {_: 0 for _ in duplicates}
                    for i, n in enumerate(names):
                        newname = self._rename(n, counter, duplicates[n])

                        # separate variables and global attributes
                        if newname in self._as_attributes():
                            attrs[newname] = entry.data[0, i]
                            continue

                        fieldnames.append(newname)
                        fieldindices.append(i)

                    units = entry.units

                recdata.append(entry.data)

        del _handler

        # normalize units
        for i, u in enumerate(units):
            if u in UNITS:
                units[i] = UNITS[u]

        # concatenate data and mask fill values
        recdata = numpy.ma.masked_equal(recdata, FILLVALUE)

        for _, f in enumerate(fieldnames):
            shape = list(recdata.shape)
            # remove field indice dimensension
            shape.pop(1)
            if ientry == 1:
                data[f] = recdata[:, ..., fieldindices[_]]
            else:
                data[f] = numpy.ma.reshape(
                    recdata[:, fieldindices[_]],
                    (irec, ientry,)
                )

        # get dimensions
        nbrows, nbcells = data['lat'].shape
        dims = OrderedDict([
            (self._native2std_dim['dim_rec'], nbrows),
            (self._native2std_dim['dim_entry'], nbcells)
        ])

        # get fields and cache data
        fields = {}
        for i, fieldname in enumerate(fieldnames):
            if fieldname in ['lat', 'lon', 'year', 'month', 'day', 'hour',
                             'minute', 'second']:
                continue
            varattrs = {
                'long_name': fieldname.replace('_', ' '),
                'units': units[fieldindices[i]],
            }
            varencoding = {
                '_FillValue': FILLVALUE
            }
            varobj = xr.DataArray(
                data=data[fieldname],
                dims=tuple(dims.keys()),
                attrs=varattrs
            )
            varobj.encoding.update(varencoding)
            fields[fieldname] = varobj

        # geolocation
        coords = {}
        coords['lat'] = xr.DataArray(
            name='lat',
            data=data['lat'],
            dims=dims,
            attrs={
                'units': 'degrees_north',
                'long_name': 'latitude'
            },
        )
        coords['lat'].encoding.update({'_FillValue': FILLVALUE})

        coords['lon'] = xr.DataArray(
            name='lon',
            data=data['lon'][:],
            dims=dims,
            attrs={
                'units': 'degrees_east',
                'long_name': 'longitude'
            },
        )
        coords['lon'].encoding.update({'_FillValue': FILLVALUE})

        times = (
            data['year'].astype(int).astype(str).astype(numpy.datetime64) +
            (data['month'].astype(int) - 1).astype('timedelta64[M]') +
            (data['day'].astype(int) - 1).astype('timedelta64[D]') +
            (data['hour'].astype(int)).astype('timedelta64[h]') +
            (data['minute'].astype(int)).astype('timedelta64[m]') +
            (data['second'].astype(int)).astype('timedelta64[s]')
        )
        coords['time'] = xr.DataArray(
            name='time',
            data=times,
            dims=dims,
            attrs={
                'long_name': 'time'
            }
        )
        coords['time'].encoding.update({'_FillValue': -1})
        coords['time'].encoding['units'] = 'seconds since {}'.format(
            REFERENCE_TIME.strftime("%Y-%m-%d %H:%M:%S")
        )

        # build xarray dataset
        xrdataset = xr.Dataset(
            coords={'lat': coords['lat'], 'lon': coords['lon'],
                    'time': coords['time']},
            data_vars=fields,
            attrs=attrs
        )

        return xrdataset
