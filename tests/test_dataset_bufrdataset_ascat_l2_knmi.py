"""
Test class for cerbere BUFRDataset
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import unittest

from tests.checker import Checker


class BUFRDatasetChecker(Checker, unittest.TestCase):
    """Test class for BUFRDataset swath files"""

    def __init__(self, methodName="runTest"):
        super(BUFRDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'BUFRDataset'

    @classmethod
    def feature(cls):
        """Return the related datamodel class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "ascat_20191211_130900_metopb_37518_eps_o_057_ovw.l2_bufr"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"

