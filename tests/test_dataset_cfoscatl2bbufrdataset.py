"""
Test class for cerbere BUFRDataset
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import unittest

from tests.checker import Checker


class CFOSCATL2BBUFRDatasetChecker(Checker, unittest.TestCase):
    """Test class for CFOSCATL2BBUFRDataset swath files"""

    def __init__(self, methodName="runTest"):
        super(CFOSCATL2BBUFRDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'CFOSCATL2BBUFRDataset'

    @classmethod
    def feature(cls):
        """Return the related datamodel class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "rfsca_20200403_032101_cfosat_07909_o_250_ovw_l2.bufr"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"

