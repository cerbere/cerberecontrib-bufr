# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


requires = ['cerbere>=2.0.0']

setup(
    name='cerberecontrib-bufr',
    version='1.0',
    url='',
    download_url='',
    license='GPLv3',
    author='Jeff Piolle',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for BUFR format',
    long_description="This package contains the Cerbere extension for BUFR format.",
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    entry_points={
        'cerbere.plugins': [
            'BUFRDataset = cerberecontrib_bufr.dataset.bufrdataset:BUFRDataset',
            'CFOSCATL2BBUFRDataset = cerberecontrib_bufr.dataset.cfoscatl2bbufrdataset:CFOSCATL2BBUFRDataset',
            'KNMIScatBUFRDataset = cerberecontrib_bufr.dataset.knmiscatbufrdataset:KNMIScatBUFRDataset',

        ]
    },
    platforms='any',
    packages=find_packages(include=['cerberecontrib_bufr.*']),
    include_package_data=True,
    package_data={},
    #install_requires=requires,
)
