cerberecontrib-bufr
===================

Contrib cerbere package for BUFR data.

Requires:
  * pybufr-ecmwf


Tested products
---------------

  * KNMI ASCAT L2 products at 5.7 km [``BUFRDataset``, ``Swath``]
  * CFOSAT SCAT products generated with KNMI CWDP [``CFOSCATL2BUFRDataset``, ``Swath``]